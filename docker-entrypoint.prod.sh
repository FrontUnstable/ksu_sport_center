#!/usr/bin/env bash
sleep 10

echo Collecting static
python src/manage.py collectstatic --noinput  # Collect static files
echo Migrations initialize.
python src/manage.py migrate                  # Apply database migrations
echo Load db data.
python src/manage.py loaddata db.json

echo Starting Gunicorn.

exec gunicorn sc_server.wsgi -w 4 -b 0.0.0.0:8000 --chdir=/code/src --reload
