from django.apps import AppConfig

from django.utils.translation import ugettext_lazy as _


class ScCommonConfig(AppConfig):
    name = 'sc_common'
    verbose_name = _("Загальний додаток")
