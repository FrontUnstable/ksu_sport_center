import json

from django.contrib.auth import get_user_model, authenticate, login
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.shortcuts import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.views.generic.edit import BaseFormView

from sc_auth.forms import LoginForm, SignUpForm
from sc_core.utils import SubmitCSFRProtectMixin

from sc_auth.services.UserService import is_user_exist

User = get_user_model()


# logger = logging.getLogger('sc_auth')


class LoginView(SubmitCSFRProtectMixin, BaseFormView):
    form_class = LoginForm

    def form_valid(self, form):
        try:
            login_username = form.cleaned_data['login_username']
            password = form.cleaned_data['login_password']
            if login_username and password:
                user = authenticate(request=self.request, username=login_username, password=password)
                if not user:
                    raise ValidationError(_("Користувача з таким логіном не існує"))
                if not user.is_active:
                    raise (_("User is not active. Activate letter sent"))
                login(self.request, user)
                return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
        except ValidationError as e:
            for msg in e.messages:
                messages.error(self.request, msg)
            return HttpResponseRedirect(reverse('pages-root'))
        except Exception as e:
            messages.error(self.request, e)
            return HttpResponseRedirect(reverse('pages-root'))

    def post(self, request, *args, **kwargs):
        self.kwargs = kwargs
        return super(LoginView, self).post(request, *args, **kwargs)


def get(self, request, *args, **kwargs):
    return HttpResponseRedirect(reverse('pages-root'))


class SignUpView(SubmitCSFRProtectMixin, BaseFormView):
    form_class = SignUpForm

    def form_valid(self, form):
        try:
            email = form.cleaned_data['signup_email']
            password = form.cleaned_data['signup_password']
            username = form.cleaned_data['signup_username']
            first_name = form.cleaned_data['signup_first_name']
            last_name = form.cleaned_data['signup_last_name']
            phone_number = form.cleaned_data['phone_number']
            user_exist, result = is_user_exist(email=email, username=username)
            if user_exist:
                messages.error(self.request, result)
                return HttpResponseRedirect(reverse('pages-root'))
            user = User.objects.create(email=email,
                                       password=password,
                                       username=username,
                                       phone_number=phone_number,
                                       first_name=first_name,
                                       last_name=last_name,
                                       is_active=True)
            user.is_active = True
            user.save()
            login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
            return HttpResponseRedirect(reverse('pages-root'))
        except ValidationError as e:
            for msg in e.messages:
                messages.error(self.request, msg)
            return HttpResponseRedirect(reverse('pages-root'))
        except Exception as e:
            messages.error(self.request, e)
            return HttpResponseRedirect(reverse('pages-root'))

    def render_to_response(self, context):
        for key, error in context.get('form').errors.items():
            if key == "__all__":
                messages.error(self.request, error)
            else:
                messages.error(self.request, "%s:%s" % (key, error))
        return HttpResponseRedirect(reverse('pages-root'))

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('pages-root'))
