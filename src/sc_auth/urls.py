from django.conf.urls import include, url
from django.contrib.auth.views import LogoutView

from sc_auth.views import LoginView, SignUpView


urlpatterns = [
    url(r'^login', LoginView.as_view(), name='login'),
    url(r'^sign_up$', SignUpView.as_view(), name='sign_up'),
    url(r'^logout', LogoutView.as_view(), name='logout'),
]
