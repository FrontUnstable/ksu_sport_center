from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class CustomUser(AbstractUser):
    subscriptions = models.ManyToManyField("sc_clubs.SportClub", verbose_name=_("subscriptions"), blank=True,
                                           through="UserClubSubscription")
    phone_number = PhoneNumberField(verbose_name=_("Номер телефону"))

    class Meta:
        verbose_name = _("Користувач")
        verbose_name_plural = _("Користувачi")

    def __str__(self):
        return self.username + " %s (%s - %s)" % (self.get_full_name(), self.email, self.phone_number)


class UserClubSubscription(models.Model):
    sportclub = models.ForeignKey("sc_clubs.SportClub", on_delete=models.CASCADE, verbose_name="Секція")
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name="Користувач")
    comment = models.TextField(max_length=256, editable=False, null=True, blank=True, verbose_name=_("Коментарій"))

    created_at = models.DateTimeField(auto_now_add=True)
    is_processed = models.BooleanField(default=False, verbose_name=_("Оброблено"))

    class Meta:
        unique_together = ("sportclub", "user")
        verbose_name = _("Пiдписник")
        verbose_name_plural = _("Пiдписники")

    def __str__(self):
        return "%s  - %s" % (self.sportclub.name, self.user)
