from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from sc_news.services.NewsService import get_all_news, get_news_by_sport_club, get_news_by_sport_object


class NewsBigList(CMSPluginBase):
    module = _("Новини")
    name = _("Новини список")
    render_template = "sc_news/plugins/news_big_list_plugin.html"

    def render(self, context, instance, placeholder):
        context["news_list"] = get_all_news()[:20]
        return context

    def get_render_template(self, context, instance, placeholder):
        return self.render_template


class NewsSmallList(CMSPluginBase):
    module = _("Новини")
    name = _("Новини останнi 5 за об'єктом або секцією")
    render_template = "sc_news/plugins/news_small_list_plugin.html"

    def render(self, context, instance, placeholder):
        if "sport_object" in context:
            news = get_news_by_sport_object(context.get("sport_object"))
        elif "sport_club" in context:
            news = get_news_by_sport_club(context.get("sport_club"))
        else:
            news = get_all_news()
        context["news_list"] = news[:4]
        return context

    def get_render_template(self, context, instance, placeholder):
        return self.render_template


class NewsDetail(CMSPluginBase):
    module = _("Новини")
    name = _("Новина - детально")
    render_template = "sc_news/plugins/news_details_plugin.html"


plugin_pool.register_plugin(NewsBigList)
plugin_pool.register_plugin(NewsSmallList)
plugin_pool.register_plugin(NewsDetail)
