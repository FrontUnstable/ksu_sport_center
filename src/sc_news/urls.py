from django.conf.urls import url

from sc_news.views import *

urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/', NewsDetail.as_view(), name='NewsDetail'),
    url(r'^', NewsList.as_view(), name='NewsList'),
]
