from django.contrib import admin
from sc_news.models import News


# Register your models here.
class NewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'sportclub', 'sportobject')
    prepopulated_fields = {'slug': ('name',)}
    fieldsets = (
        ('Object info', {
            'fields': (('sportclub', 'sportobject'), 'name', 'short_description', 'description')
        }),
        ('Additional', {
            'fields': ('slug',)
        }),
        ('Media', {
            'fields': ('image',)
        }),
    )


admin.site.register(News, NewsAdmin)
