from django.db import models
from filer.fields.image import FilerImageField
from sc_clubs.models import SportClub, SportObject
from dynamic_validator import ModelFieldRequiredMixin
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField
from django.urls import reverse


# Create your models here.
class News(ModelFieldRequiredMixin, models.Model):
    sportclub = models.ForeignKey(SportClub, on_delete=models.SET_NULL, null=True, blank=True)
    sportobject = models.ForeignKey(SportObject, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=32, db_index=True,unique=True, verbose_name=_("Назва"))
    description = HTMLField(max_length=2048, db_index=True, null=True, blank=True,
                            verbose_name=_("Опис"))

    short_description = models.CharField(max_length=128, db_index=True, null=True, blank=True,
                                         verbose_name=_("Короткий опис"))
    slug = models.SlugField(max_length=32, unique=True, db_index=True, verbose_name="Slug (URL)")
    image = FilerImageField(null=True, blank=True, on_delete=models.SET_NULL, related_name="news_banners",
                            verbose_name=_("Зображення"))
    created_at = models.DateTimeField(auto_now_add=True)

    REQUIRED_TOGGLE_FIELDS = [
        ["sportclub", "sportobject"],  # django-dynamic-model-validation
    ]

    class Meta:
        ordering = ['name']
        verbose_name = _("Новина")
        verbose_name_plural = _("Новини")

    def __str__(self):
        return self.name

    def get_url(self):
        return reverse('news:NewsDetail', args=[self.slug])
