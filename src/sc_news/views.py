from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from sc_news.models import News
from sc_news.services.NewsService import get_all_news


class NewsList(ListView):
    model = News
    context_object_name = "news_list"
    template_name = "sc_news/news_list.html"

    def get_context_data(self, **kwargs):
        context = {"news_list": get_all_news()}
        context.update(kwargs)
        return super().get_context_data(**context)


# View of list of all lots
class NewsDetail(DetailView):
    model = News
    context_object_name = "news"
    template_name = "sc_news/news_details.html"

    def get_context_data(self, **kwargs):
        context = {"meta_title": self.object.name}
        context.update(kwargs)
        return super().get_context_data(**context)
