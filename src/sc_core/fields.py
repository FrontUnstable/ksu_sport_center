from djangocms_attributes_field.fields import AttributesField
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import ugettext as _
import re

regex_key_validator = RegexValidator(regex=r'^[A-zА-я][-a-zа-я0-9_]*\Z',
                                     flags=re.IGNORECASE, code='invalid')


class CustomAttributesField(AttributesField):
    def validate_key(self, key):
        """
        A key must start with a letter, but can otherwise contain letters,
        numbers, dashes or underscores. It must not also be part of
        `excluded_keys` as configured in the field.

        :param key: (str) The key to validate
        """
        # Verify the key is not one of `excluded_keys`.
        if key.lower() in self.excluded_keys:
            raise ValidationError(
                _('"{key}" is excluded by configuration and cannot be used as '
                  'a key.').format(key=key))
        # Also check that it fits our permitted syntax
        try:
            regex_key_validator(key)
        except ValidationError:
            # Seems silly to catch one then raise another ValidationError, but
            # the RegExValidator doesn't use placeholders in its error message.
            raise ValidationError(
                _('"{key}" is not a valid key. Keys must start with at least '
                  'one letter and consist only of the letters, numbers, '
                  'underscores or hyphens.').format(key=key))
