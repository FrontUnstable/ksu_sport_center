class SCException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "[ERROR] %s\n" % str(self.message)


class NotFound(SCException):
    pass


class UnexceptedError(SCException):
    def log(self):
        ret = "Unexcepted error"
        if (hasattr(self, "reason")):
            ret = "".join([ret, "\n==> %s" % str(self.reason)])
        return ret


class PermissionException(SCException):
    pass


class ValidationException(SCException):
    pass


class ValueException(SCException):
    pass


class OverIteratedException(SCException):
    pass
