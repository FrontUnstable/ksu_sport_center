from django import forms

from django.utils.translation import ugettext as _


class SubscribeForm(forms.Form):
    comment = forms.CharField(max_length=256,
                              widget=forms.Textarea(attrs={'placeholder': _('Комментар')}), label=False)
