from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class ClubsApphook(CMSApp):
    app_name = "sc_clubs"
    name = _('Додаток "Гуртки"')

    def get_urls(self, page=None, language=None, **kwargs):
        return ["sc_clubs.urls"]


apphook_pool.register(ClubsApphook)
