# Generated by Django 2.2.6 on 2019-11-04 02:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import djangocms_text_ckeditor.fields
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('sc_clubs', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sportclub',
            name='banner_image',
        ),
        migrations.RemoveField(
            model_name='sportclub',
            name='preview_image',
        ),
        migrations.RemoveField(
            model_name='sportobject',
            name='banner_image',
        ),
        migrations.RemoveField(
            model_name='sportobject',
            name='preview_image',
        ),
        migrations.AddField(
            model_name='sportclub',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='club_banners', to=settings.FILER_IMAGE_MODEL, verbose_name='Зображення'),
        ),
        migrations.AddField(
            model_name='sportobject',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='object_banners', to=settings.FILER_IMAGE_MODEL, verbose_name='Зображення'),
        ),
        migrations.AlterField(
            model_name='sportclub',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(blank=True, db_index=True, max_length=2048, null=True, verbose_name='Опис'),
        ),
        migrations.AlterField(
            model_name='sportclub',
            name='name',
            field=models.CharField(db_index=True, max_length=64, verbose_name='Назва'),
        ),
        migrations.AlterField(
            model_name='sportclub',
            name='slug',
            field=models.SlugField(max_length=64, verbose_name='Slug (URL)'),
        ),
        migrations.AlterField(
            model_name='sportobject',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(blank=True, db_index=True, max_length=2048, null=True, verbose_name='Опис'),
        ),
        migrations.AlterField(
            model_name='sportobject',
            name='name',
            field=models.CharField(db_index=True, max_length=64, verbose_name='Назва'),
        ),
        migrations.AlterField(
            model_name='sportobject',
            name='slug',
            field=models.SlugField(max_length=64, verbose_name='Slug (URL)'),
        ),
    ]
