from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ScClubsConfig(AppConfig):
    name = 'sc_clubs'
    verbose_name = _("Спортивнi гуртки та об'єкти")

    def ready(self):
        from django.db.models.signals import post_save
        from sc_clubs.models import SportClub
        from sc_clubs.signals import create_schedule_days_for_club

        post_save.connect(create_schedule_days_for_club, sender=SportClub, weak=False)
