from django.db import models
from filer.fields.image import FilerImageField
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from djangocms_text_ckeditor.fields import HTMLField

from datetime import time


class SportObject(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, db_index=True, unique=True, verbose_name=_("Назва"))
    short_description = models.CharField(max_length=128, db_index=True, null=True, blank=True,
                                         verbose_name=_("Короткий опис"))
    description = HTMLField(max_length=2048, db_index=True, null=True, blank=True,
                            verbose_name=_("Опис"))
    slug = models.SlugField(max_length=32, db_index=True, unique=True, verbose_name="Slug (URL)")
    image = FilerImageField(null=True, blank=True, on_delete=models.SET_NULL, related_name="object_banners",
                            verbose_name=_("Зображення"))

    class Meta:
        ordering = ['name']
        verbose_name = _("Спортивний об'єкт")
        verbose_name_plural = _("Спортивнi об'єкти")

    def __str__(self):
        return self.name

    def get_url(self):
        return reverse('clubs:ObjectDetail', args=[self.slug])


class SportClub(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, db_index=True, verbose_name=_("Назва"))
    short_description = models.CharField(max_length=128, unique=True, db_index=True, null=True, blank=True,
                                         verbose_name=_("Короткий опис"))
    description = HTMLField(max_length=2048, db_index=True, null=True, blank=True,
                            verbose_name=_("Опис"))
    slug = models.SlugField(max_length=32, db_index=True, unique=True, verbose_name="Slug (URL)")
    image = FilerImageField(null=True, blank=True, on_delete=models.SET_NULL, related_name="club_banners",
                            verbose_name=_("Зображення"))
    sport_object = models.ForeignKey(SportObject, null=True, on_delete=models.SET_NULL, related_name='lots',
                                     verbose_name=_("Спортивний об'єкт"))

    class Meta:
        ordering = ['name']
        verbose_name = _("Спортивний гурток")
        verbose_name_plural = _("Спортивнi гуртки")
        index_together = [
            ['id', 'slug'],
        ]

    def __str__(self):
        return self.name

    def get_url(self):
        return reverse('clubs:ClubDetail', args=[self.sport_object.slug, self.slug])

    def schedule(self):
        return self.schedule_days.all().order_by("day_order")


class ClubScheduleDay(models.Model):
    id = models.AutoField(primary_key=True)
    day_name = models.CharField(max_length=10, db_index=True, verbose_name=_("Назва"))
    sport_club = models.ForeignKey(SportClub, null=True, on_delete=models.SET_NULL, related_name='schedule_days',
                                   verbose_name=_("Спортивна секція"))
    datetime_from = models.TimeField(null=True, blank=True, verbose_name=_("Початок роботи"),
                                     default=time(hour=8, minute=30))
    datetime_to = models.TimeField(null=True, blank=True, verbose_name=_("Кінець роботи"),
                                   default=time(hour=16, minute=00))
    is_able = models.BooleanField(default=True, verbose_name=_("Секція працює"))
    day_order = models.IntegerField()

    class Meta:
        ordering = ['day_order']
        verbose_name = _("День графіку")
        verbose_name_plural = _("Графік роботи")

    def __str__(self):
        return self.day_name

