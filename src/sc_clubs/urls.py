from django.conf.urls import url

from sc_clubs.views import *

urlpatterns = [
    url(r'^(?P<object_slug>[\w-]+)/(?P<slug>[\w-]+)/subscribe', SubscriptionView.as_view(), name='ClubSubscribe'),
    url(r'^(?P<object_slug>[\w-]+)/(?P<slug>[\w-]+)', SportClubDetail.as_view(), name='ClubDetail'),
    url(r'^(?P<slug>[\w-]+)/', SportObjectDetail.as_view(), name='ObjectDetail'),
    url(r'^', SportObjectList.as_view(), name='ObjectsList'),
   ]
