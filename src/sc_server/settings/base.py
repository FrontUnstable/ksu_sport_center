"""
Django settings for sc_server project.

Generated by 'django-admin startproject' using Django 2.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

# SECURITY WARNING: don't run with debug turned on in production!
# Application definition
AUTH_USER_MODEL = "sc_auth.CustomUser"

INSTALLED_APPS = [
    'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'sc_auth',
    'cms',
    'sekizai',
    'menus',
    'treebeard',
    'filer',
    'easy_thumbnails',
    'mptt',
    'phonenumber_field',
    'djangocms_text_ckeditor',
    'djangocms_snippet',
    'djangocms_style',
    'djangocms_attributes_field',
    'djangocms_video',
    'djangocms_picture',
    'sc_core.apps.ScCoreConfig',
    'sc_clubs',
    'sc_common',
    'sc_news',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'sc_auth.middleware.ClubSubscriptorsCount',
]

ROOT_URLCONF = 'sc_server.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), 'templates', os.path.join(BASE_DIR, '../../../', 'templates'), ]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'sekizai.context_processors.sekizai'
            ],
        },
    },
]

WSGI_APPLICATION = 'sc_server.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'uk'

LANGUAGES = [
    ('uk', _('Українська')),
#    ('en', _('English')),
#    ('ru', _('Русский')),
]

LOCALE_PATHS = [
    os.path.join(BASE_DIR, '../locale'),
    os.path.join(BASE_DIR, 'locale'),
    '/var/local/translations/locale',
    'locale',
]

CMS_LANGUAGES = {
    1: [
    #    {
    #        'code': 'en',
    #        'name': _('English'),
    #        'public': True,
    #    },
        {
            'code': 'uk',
            'name': _('Українська'),
            'public': True,
            'fallbacks': ['en'],
        },
    #    {
    #        'name': _('Русский'),
    #        'public': True,
    #        'code': 'ru',
    #        'fallbacks': ['en'],
    #    },
    ],
    'default': {
        'fallbacks': ['uk'],
        'redirect_on_fallback': False,
        'public': True,
        'hide_untranslated': False,
    },
}

SITE_ID = 1

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True


#STATICFILES_DIRS = (
#    os.path.join(BASE_DIR, 'static'),
#)

CMS_TEMPLATES = [
    ('sc_core/index.html', 'Home page template'),
    ('sc_clubs/sport_objects_list.html', 'Sport clubs template'),
    ('sc_news/news_list.html', 'News template'),
]

THUMBNAIL_HIGH_RESOLUTION = True

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters'
)

CMS_PAGE_CACHE=False
DJANGOCMS_PICTURE_NESTING = True

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',)
