#!/usr/bin/env bash
sleep 10

echo Making migrations.
python src/manage.py makemigrations
echo Migrations initialize.
python src/manage.py migrate                  # Apply database migrations
echo Load db data.
python src/manage.py loaddata db.json

echo Starting Gunicorn.

exec python /code/src/manage.py runserver 0.0.0.0:8000
